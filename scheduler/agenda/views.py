# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext

from models import ItemAgenda
from forms import FormItemAgenda

def index(request):
	return HttpResponse(u"Olá Mundo!!")

def list(request):
	lista_itens = ItemAgenda.objects.all()
	return render_to_response("list.html", {'lista_itens': lista_itens})

def add(request):
	if request.method == 'POST': # form was sended
		form = FormItemAgenda(request.POST, request.FILES)
		if form.is_valid():
			# Valid form
			data = form.cleaned_data
			item = ItemAgenda(date=data['date'], hour=data['hour'], title=data['title'], description=data['description'])
			item.save()
			
			# registered form message
			return render_to_response("save.html", {})
	else: # site was accesed via link (GET Method)
		# show empty form
		form = FormItemAgenda()

	return render_to_response("add.html", {'form':form}, context_instance=RequestContext(request))

