# -*- coding: utf-8 -*-

from django import forms

class FormItemAgenda(forms.Form):
	title = forms.CharField(max_length=100)
	date  = forms.DateField(widget=forms.DateInput(format='%d/%m/%Y'), input_formats=['%d/%m/%y', '%d/%m/%Y'])
	hour  = forms.TimeField()
	description = forms.CharField()

